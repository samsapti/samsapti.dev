# My personal website

[![Build Status](https://drone.data.coop/api/badges/samsapti/samsapti.dev/status.svg)](https://drone.data.coop/samsapti/samsapti.dev)

This is the source code of my [personal website](https://samsapti.dev)
([onion service](http://mldhltdackluvnqso7vk2azcg3ghjxbpw4im6alubymqkonb4kppqcqd.onion/)).

## License

- All content is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0).
- All code is licensed under [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html).
