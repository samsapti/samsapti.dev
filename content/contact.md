---
title: Contact Information
---

## Email

I have two different email addresses:

- [sam@sapti.me](mailto:sam@sapti.me) - This is my primary email
    address.
- [sam@data.coop](mailto:sam@data.coop) - This is my
    [data.coop](https://data.coop) email address. Please only use this email
    address for communication related to [data.coop](https://data.coop).

If you want to send me encrypted emails or verify my signatures, my public PGP
key is [here]({{< relref "keys.md#pgp-key" >}} "PGP key"). My email clients
support subject header encryption.

## Matrix

My Matrix username is
[@samsapti:data.coop](https://matrix.to/#/@samsapti:data.coop). If you
want to message me securely, please verify my session(s) below.

```yaml
matrix_sessions:
  - name: Cinny Desktop
    id: GVOTFNLXJH
  - name: Element X Android
    id: ITKKUPBLLZ
```

## Signal

My Signal username is [samsapti.874](https://signal.me/#eu/saR7zck1j-tVrxIMt7519fqh9b1RHlOD5M3GS9yqSIUUsoTqzqJC7bQAfsD2CWsG).

![Signal username QR code](/images/signal-username-qr-code.png)
