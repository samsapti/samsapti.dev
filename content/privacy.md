---
title: Privacy Policy
---

**_Last updated: June 24, 2023_**

*This privacy policy applies to this website, as well as the free services
mentioned in the [about page]({{< relref "about.md#free-services" >}}).*

## Who we are

This website and the free services are owned and hosted by Sam Al-Sapti.

## What data is collected

No data is collected about the site's visitors. The webserver's access logs are
discarded immediately, so the server doesn't persist any IP addresses or other
personally identifiable information. Moreover, if you access this site over a
VPN or [Tor
connection](http://mldhltdackluvnqso7vk2azcg3ghjxbpw4im6alubymqkonb4kppqcqd.onion),
the site won't even be able to learn your IP address in case of a compromise.

Furthermore, the hosting provider of this site is [Hetzner Online
GmbH](https://www.hetzner.com/). According to their privacy policy, they do not
store any log data either. Please refer to their privacy policy for further
information.

If you use my SearXNG instance however, the built-in limiter plugin will
collect your IP address in hashed form. Hashing is a one-way encryption method
that allows data to be encrypted, but not decrypted. This means that the server
does not learn your real IP address, but only a one-way encrypted version of it
so that it can detect IP addresses that behave maliciously and rate limit
connections from those. Furthermore, this database of hashed IP addresses is
not used for any other purpose than rate limiting. A single hashed IP address
is stored for a maximum of 10 minutes after the last request from it.

## Cookies

No cookies are used on this website. However, your browser's local storage is
used to save your color scheme preference if you ever change it manually.
SearXNG can optionally use cookies to store settings if you choose to change
them from the default. Your settings can alternatively be stored in a custom
URL instead.

## Embedded third party content

Currently no third party content is embedded on this site.

## Analytics

No analytics are used on this site. SearXNG measures aggregate statistics on
how upstream search engines perform, but this does not include any user data.

## Changes to this privacy policy

I reserve the right to update this privacy policy from time to time. I
constantly keep it up to date with the latest changes. If this policy is
changed substantially, I will put a clear notice on the front page for at least
7 days.
