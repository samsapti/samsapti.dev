---
title: My Cryptographic Keys
---

## PGP key

My public PGP key is the following:

```txt
pub   ed25519/0x3FC96B835B918FC3 2022-05-28 [C] [expires: 2026-05-15]
      Key fingerprint = 7D80 F5D8 4022 B8F5 E030  CC3E 3FC9 6B83 5B91 8FC3
uid                   [ unknown] Sam Al-Sapti <sam@sapti.me>
uid                   [ unknown] Sam Al-Sapti <sam@data.coop>
sub   ed25519/0xCBBBE7371E81C4EA 2022-05-28 [S] [expires: 2025-06-07]
      Key fingerprint = 758F 1A17 C803 5FD9 3912  C9E2 CBBB E737 1E81 C4EA
sub   cv25519/0x914289689CF45D4F 2022-05-28 [E] [expires: 2025-06-07]
      Key fingerprint = 20D2 BBB4 63CA 6CB6 F295  F2BA 9142 8968 9CF4 5D4F
sub   ed25519/0x899C7CF4B526656F 2022-05-28 [A] [expires: 2025-06-07]
      Key fingerprint = FA9B 317E D1D3 4906 46CC  D154 899C 7CF4 B526 656F
```

You can download it [here](/pgp.asc) or from your preferred keyserver.

<details>
  <summary>
    How I keep my private key safe
  </summary>

  ### Master key

  My private master key is only used for the following purposes:

  - Add or revoke UIDs
  - Add or revoke subkeys
  - Change expiry for subkeys or the master key itself
  - Sign other keys

  My private master key is only ever accessed on an airgapped machine, with no
  internet or wireless communication capabilities (all wireless components
  physically removed), no camera or microphone and no persistent storage. This
  airgapped machine is booted with the latest version of [Tails
  OS](https://tails.boum.org). The master key is protected by a long and secure
  passphrase and stored on an encrypted storage medium, which itself is stored
  in a safe place.

  ### Subkeys

  My subkeys are stored on an OpenPGP smartcard for daily use. The smartcard
  makes sure that the local machine never has direct access to the keys. It is
  protected by a pin-code and requires a physical touch on every cryptographic
  operation.

  ### Revocation and expiry

  I usually set my master key to be valid for 2 years at a time. I will always
  extend it before the expiry date. The same goes for my subkeys, which are set
  to be valid for 6 months at a time.

  If my keys are ever compromised, I have a revocation certificate, stored in a
  safe place, that I will publish to this website and various keyservers.

</details>

<details>
  <summary>
    Key signing policy
  </summary>

  ### Certification levels

  These are the certification levels I use to sign other keys, and the
  requirements for each level.

  #### Level 0: Generic verification (`sig`/`0x10`)

  This certification level is used if I have somehow verified that you are in
  control of the email address(es) of the UID(s) to be signed. No assertions
  are made about your identity.

  #### Level 1: No verification (`sig1`/`0x11`)

  This certification level is used when I have not safely verified you as the
  keyholder, but I merely *believe* that you own the key in question.

  #### Level 2: Casual verification (`sig2`/`0x12`)

  This certification level is used when I have verified your identity with at
  least one form of photo ID (government-issued or equally secure), that your
  identity matches that of the UID(s) to be signed, and that you are in control
  of the email address(es) of the UID(s) to be signed.

  #### Level 3: Extensive verification (`sig3`/`0x13`)

  This certification level is used when I am *absolutely sure* that you are in
  fact the keyholder. This means that either you are someone I know personally
  and trust, or that someone I ultimately trust have notified me that you want
  a signature and have given me your key fingerprint in a secure manner.

  ### Signing process

  The signing process consists of 2 steps:

  1) Verification will take place either in person or over video call. If we
  meet in person, you will give me a physical copy of your key fingerprint. If
  verification takes place over video call, you will give me your key
  fingerprint verbally.
  2) You will have to send me your public key from the email address associated
  with one of the UIDs to be signed. The email has to be signed. I will then
  sign the key and send it back to the same email address in encrypted form.

</details>

## SSH key

If you need to give me shell access to your server or similar, please use the
following public SSH key:

```txt
sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIFWZGLov8wPBNxuvnaPK+8vv6wK5hHUVEFzXKsN9QeuBAAAADHNzaDpzYW1zYXB0aQ== ssh:samsapti
```

If your SSH server does not support FIDO2-protected SSH keys, use this fallback
key instead:

```txt
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPd/4fQV7CL8/KVwbo/phiV5UdXFBIDlkZ+ps8C7FeRf cardno:14 336 332
```

<details>
  <summary>
    PGP signed version
  </summary>

  ```txt
  -----BEGIN PGP SIGNED MESSAGE-----
  Hash: SHA512

  sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIFWZGLov8wPBNxuvnaPK+8vv6wK5hHUVEFzXKsN9QeuBAAAADHNzaDpzYW1zYXB0aQ== ssh:samsapti
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPd/4fQV7CL8/KVwbo/phiV5UdXFBIDlkZ+ps8C7FeRf cardno:14 336 332

  -----BEGIN PGP SIGNATURE-----

  iHUEARYKAB0WIQR1jxoXyANf2TkSyeLLu+c3HoHE6gUCZBC37AAKCRDLu+c3HoHE
  6r2nAQCoBJvJnlUE8gv/vSRL+2YvuxDxNESKXnkdZ0RBW+USxgD9E9CE4QIFMj5U
  +COZ61aYDmdNlcmytPviQ+g2k8SefQA=
  =h3YT
  -----END PGP SIGNATURE-----
  ```
</details>

You can download my SSH key [here](/ssh.pub), and my fallback key
[here](/ssh_fallback.pub).
