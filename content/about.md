---
title: About Me
---

## Overview

I'm a software developer from Denmark and I work as an Operations Engineer. My
main interests are backend development, DevOps, DevSecOps, IT security,
open-source, server administration and Linux.

In my free time I'm an FPV drone pilot. I have a PeerTube channel over at
[@sam.fpv@makertube.net](https://makertube.net/c/sam.fpv), where I occasionally
post some footage.

Further, I'm an advocate for online privacy, I'm against attention economy and
surveillance capitalism, I'm a big supporter of the Free Software movement, I'm
a digital minimalist, and I'm a member of [data.coop](https://data.coop) (I'm
also one of the system administrators). Also, you won't find me on
[Facebook](https://fsf.org/fb) or any other Big Tech social media platform
(except for [LinkedIn](https://www.linkedin.com/in/sam-a-dev/), but that's not
really a social media platform).

## My skills

Some technologies and tech concepts I'm familiar with are:

- Programming languages:
  - Bash
  - C
  - C++
  - C#
  - F#
  - Go
  - Java
  - Python
- DevOps, CI/CD pipelines, Ansible, Puppet, cloud (DigitalOcean, Hetzner)
- Docker, virtualization software (QEMU/KVM, VirtualBox, Vagrant)
- Linux servers and IT operations
- Basic IT security principles and best practices
- Firewalls and loadbalancing
- SQL and database systems

## Free services

I host some online services that you're welcome to use free of charge.

- [SearXNG](https://search.sapti.me) -
  A metasearch engine that gets its results from other search engines while
  protecting your privacy.
