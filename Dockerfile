FROM nginx:alpine-slim

COPY nginx.conf /etc/nginx/conf.d/web.conf
COPY public/ /usr/share/nginx/html/
